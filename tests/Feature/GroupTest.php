<?php

namespace Tests\Feature;

use App\Models\Cripto;
use App\Models\Group;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GroupTest extends TestCase
{
    // use RefreshDatabase; // Limpa o banco de dados após cada teste

    public function test_index_returns_all_groups()
    {
        $response = $this->json('GET', '/api/lista-grupos');

        $response->assertStatus(200); // Código de status HTTP 200 (OK)
        $response->assertJsonStructure([
            '*' => [
                'id',
                'name',
            ],
        ]);
    }

    public function test_getGroupCripto_returns_group_criptos()
    {
        $group = Group::first();

        $response = $this->json('GET', '/api/lista-grupo-criptos/' . $group->id );

        $response->assertStatus(200); // Código de status HTTP 200 (OK)
    }

    public function test_store_validates_name_required()
    {
        $data = [
            'criptos' => [1, 2],
        ];

        $response = $this->postJson('/api/criar-grupo', $data);

        $response->assertStatus(200); // Código de status HTTP 422 (Unprocessable Entity)
        $response->assertJsonFragment(['message' => 'O campo nome é obrigatório.']);
    }
}
