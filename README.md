## Siga as instruçoes abaixo para montar e rodar o ambiente

- Faça clone do repo no link [gitlab](https://gitlab.com/moisesfausto/teste-cplug.git)

- Dentro do diretorio api/ faça uma copia do .env.example
```
cp .env.example .env
```

-- Adicione as seguintes variaveis ao .env
```
DB_CONNECTION=pgsql
DB_HOST=127.0.0.1
DB_PORT=5432
DB_DATABASE=api
DB_USERNAME=root
DB_PASSWORD=root

CMC_PRO_API_KEY="sua chave de acesso coinmarket"
```

- Dentro do diretorio api/ rode o comando:
```
docker compose up -d
```

- Coloque o servidor para rodar com o comando:
```
php artisan serve
```

- Apos a imagem ser criada rode o comando:
```
php artisan migrate
```

- Assim que tiver criado as tabelas, vamos popular
as mesmas, usando o comando abaixo:
```
php artisan update:cripto
```

- para rodar os teste, digite o comando:

```
php artisan test
```

## Rodar ambiente de Front

- Rode o comando para instalar as dependencias:
```
npm i install
```

- Agora rode o comando para rodar o servidor
```
npm run dev
```

### Rotas Web
- home: http://127.0.0.1:8000/
- ao fazer o cadastro é redirecionado para: /grupos/id

### Rotas Api
- (/api/lista-criptos)
- (/api/lista-grupo-criptos/2)
- (/api/lista-grupos)
- (/api/criar-grupo)

Enpoint de cadastro espera receber o seguinte Json
```
{
	"name": "Meu Grupo XPTO",
	"criptos": [
		1,
		2,
		3
	]
}
```

- [@moisesfausto](https://beacons.ai/moisesfausto)
