import './bootstrap';

import { createApp} from "vue";
import App from './components/App.vue';
import Groups from './components/Groups.vue';

const app = createApp();

app.component('app', App);
app.component('groups', Groups)

app.mount('#app');
