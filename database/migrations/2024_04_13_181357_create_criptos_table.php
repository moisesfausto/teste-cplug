<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('criptos', function (Blueprint $table) {
            $table->id();
            $table->string('name', 20);
            $table->float('market_cap');
            $table->float('price');
            $table->float('volume_24h');
            $table->float('percent_change_24h');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('criptos');
    }
};
