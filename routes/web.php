<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('home');
});

Route::get('/grupos/{id}', function () {
    return view('groups');
})->name('groups');
