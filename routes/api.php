<?php

use App\Http\Controllers\GroupController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CriptoController;

Route::get('/lista-criptos', [CriptoController::class, 'index'])->name('lista-criptos');
Route::get('/lista-grupos', [GroupController::class, 'index'])->name('lista-grupo');
Route::get('/lista-grupo-criptos/{id}', [GroupController::class, 'getGroupCripto'])->name('lista-grupo-criptos');
Route::post('/criar-grupo', [GroupController::class, 'store'])->name('criar-grupo');
