# Define a imagem base
FROM php:8.3-fpm as api

# Atualiza o repositório e instala dependências
RUN apt-get update && apt-get install -y \
    git \
    unzip \
    libpq-dev \
    && docker-php-ext-install pdo pdo_pgsql

# Define o diretório de trabalho
WORKDIR /var/www/html

RUN chmod -R 777 /var/www/html

# Instala o Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Copia os arquivos do projeto para o contêiner
COPY . .

# Instala as dependências do Composer
RUN composer create-project laravel/laravel api

# Expõe a porta 9000 para conexões
EXPOSE 8000

# Comando para iniciar o servidor PHP-FPM
CMD ["php-fpm"]
