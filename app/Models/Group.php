<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Group extends Model
{
    protected $fillable = [
        'name'
    ];

    protected $hidden = [
      'created_at',
      'updated_at'
    ];

    public function criptos(): BelongsToMany
    {
        return $this->belongsToMany(Cripto::class, 'criptos_groups');
    }
}
