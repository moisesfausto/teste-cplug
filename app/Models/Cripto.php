<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Cripto extends Model
{
    protected $fillable = [
        'name',
        'market_cap',
        'price',
        'volume_24h',
        'percent_change_24h',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function groups(): BelongsToMany
    {
        return $this->belongsToMany(Group::class, 'criptos_groups');
    }
}
