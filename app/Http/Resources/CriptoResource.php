<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Number;

class CriptoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'market_cap' => Number::format($this->market_cap, 2),
            'price' => Number::format($this->price, 2),
            'volume_24h' => Number::format($this->volume_24h, 2),
            'percent_change_24h' => Number::format($this->percent_change_24h, 2),
        ];
    }
}
