<?php

namespace App\Http\Resources;

use App\Models\Group;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Cripto;

class GroupResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $nameGroup = Group::where('id', $this->pivot->group_id)->first();

        return [
            'id_group' => $nameGroup->id,
            'name_group' => $nameGroup->name,
            'criptos' => [
                "name_group" => $this->pivot->group_id,
                "cripto_id" => $this->id,
                "name" => $this->name,
                "market_cap" => $this->market_cap,
                "price" => $this->price,
                "volume_24h" => $this->volume_24h,
                "percent_change_24h" => $this->percent_change_24h,
            ]
        ];
    }
}
