<?php

namespace App\Http\Controllers;

use App\Http\Requests\GroupPostRequest;
use App\Http\Resources\GroupResource;
use App\Models\Group;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $groups = Group::get();

        return response()->json($groups);
    }

    public function getGroupCripto($id)
    {
        $groupCriptos = Group::find($id)->criptos;

        return GroupResource::collection($groupCriptos);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): JsonResponse
    {
        try {
            $request->validate(
                ['name' => 'required|unique:groups|max:20']
            );

            $group = Group::firstOrCreate(['name' => $request->name]);

            $group->criptos()->sync($request->criptos);

            return response()->json($group);
        } catch (ValidationException $validationException) {
            return response()->json(['message' => 'O campo nome é obrigatório.']);
        }
    }
}
