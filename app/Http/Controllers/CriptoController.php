<?php

namespace App\Http\Controllers;

use App\Http\Resources\CriptoResource;
use App\Models\Cripto;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class CriptoController extends Controller
{
    /**
     * @description Mostrar todas as cripto moedas
     */
    public function index()
    {
        $criptos = Cripto::get();

        return CriptoResource::collection($criptos);
    }

    /**
     * @description Atualiza a tabela com os dados da API
     */
    public static function update(): void
    {
        $response = (new CriptoController)->getCoinMarketCap();

        $arrayCripto = collect();

        foreach ($response['data'] as $key => $data) {
            $arrayCripto[$key] = [
                'name' => $data['name'],
                'market_cap' => $data['quote']['USD']['market_cap'],
                'price' => $data['quote']['USD']['price'],
                'volume_24h' => $data['quote']['USD']['volume_24h'],
                'percent_change_24h' => $data['quote']['USD']['percent_change_24h'],
            ];
        }

        foreach ($arrayCripto as $item) {
            Cripto::upsert([$item],
                uniqueBy: ['id'],
                update: ['market_cap', 'price', 'volume_24h', 'percent_change_24h']
            );
        }
    }

    /**
     * @description Faz a consulta e retorna um array com as criptomoedas
     */
    private function getCoinMarketCap()
    {
        // https://sandbox-api.coinmarketcap.com/v1/cryptocurrency/listings/latest
        $response = Http::withHeader('X-CMC_PRO_API_KEY', env('CMC_PRO_API_KEY'))
            ->acceptJson()
            ->get('https://sandbox-api.coinmarketcap.com/v1/cryptocurrency/listings/latest');

        if ($response->ok()) {
            return $response->json();
        }

        return $response->status();
    }
}
